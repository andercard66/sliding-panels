jQuery(document).ready(function($){

  /**
   * grid galeria
   * https://masonry.desandro.com/layout.html
   */
  var $grid = $('.grid-galllery').masonry({
    // set itemSelector so .grid-sizer is not used in layout
    itemSelector: '.grid-item',
    // use element for option
    columnWidth: '.grid-sizer',
    percentPosition: true
  });

  
  //cache DOM elements
  var projectsContainer = $('.cd-projects-container'),
    projectsPreviewWrapper = projectsContainer.find('.cd-projects-previews'),
    projectPreviews = projectsPreviewWrapper.children('li.previews-item'),
    projects = projectsContainer.find('.cd-projects'),
    navigationTrigger = $('.cd-nav-trigger'),
    navigationPageContainer= $('.cd-project-page-container'),
    navigation = $('.cd-primary-nav');

  var animating = false,
    //will be used to extract random numbers for projects slide up/slide down effect
    numRandoms = projects.find('li.cd-projects-item').length, 
    uniqueRandoms = [];

  //open project
  projectsPreviewWrapper.on('click', 'a', function(event){
    event.preventDefault();
    $('.home-logo').fadeOut();
    if( animating == false ) {
      animating = true;
      // esconder paginación
      $('.pagination').fadeOut();
      // recargar layout de la galeria
      setTimeout((event) => {
        $grid = $('.grid-galllery').css('opacity','1');
        $grid.masonry('reloadItems');
        $grid.masonry('layout');
      }, 1000);
      navigationTrigger.add(projectsContainer).addClass('project-open');
      openProject($(this).parent('li.previews-item'));
    }
  });

  navigationTrigger.on('click', function(event){
    event.preventDefault();
    if( animating == false ) {
      animating = true;
      // mostrar paginación
      $('.pagination').fadeIn();
      if( navigationTrigger.hasClass('project-open') ) {
        //close visible project
        navigationTrigger.add(projectsContainer).removeClass('project-open');
        // mostrar logo
        $('.home-logo').fadeIn();
        closeProject();
      } else if( navigationTrigger.hasClass('nav-visible') ) {
        //close main navigation
        navigationTrigger.removeClass('nav-visible');
        navigation.removeClass('nav-clickable nav-visible');
        slideToggleProjects(projectsPreviewWrapper.children('li.previews-item'), -1, 0, false);
      } else {
        //open main navigation
        navigationTrigger.toggleClass('nav-visible');
        navigation.toggleClass('nav-visible');
        navigationPageContainer.toggleClass('activo');
        // esconder paginación
        $('.pagination').fadeOut();
        slideToggleProjects(projectsPreviewWrapper.children('li.previews-item'), -1, 0, true);
      }
    }else {
      navigationTrigger.toggleClass('nav-visible');
      navigation.toggleClass('nav-visible');
      navigationPageContainer.toggleClass('activo');
    }
  });

  //scroll down to project info
  projectsContainer.on('click', '.scroll', function(){
    projectsContainer.animate({'scrollTop':$(window).height()}, 500); 
  });

  //check if background-images have been loaded and show project previews
  projectPreviews.children('a').bgLoaded({
      afterLoaded : function(){
         showPreview(projectPreviews.eq(0));
      }
  });

  function showPreview(projectPreview) {
    if(projectPreview.length > 0 ) {
      setTimeout(function(){
        projectPreview.addClass('bg-loaded');
        showPreview(projectPreview.next());
      }, 150);
    }
  }

  function openProject(projectPreview) {
    var projectIndex = projectPreview.index();
    if( numRandoms == 1 ){
      projects.children('li.cd-projects-item').addClass('selected');
    }else {
      projects.children('li.cd-projects-item').eq(projectIndex).add(projectPreview).addClass('selected');
    }
    slideToggleProjects(projectPreviews, projectIndex, 0, true);
  }

  function closeProject() {
    projects.find('.selected').removeClass('content-visible').removeClass('selected')
    slideToggleProjects(projectsPreviewWrapper.children('li.previews-item'), -1, 0, false);
  }

  function slideToggleProjects(projectsPreviewWrapper, projectIndex, index, bool) {
    // slideToggleProjects(projectPreviews = selector, projectIndex = indice, 0, true);
    if(index == 0 ) createArrayRandom();
    if( projectIndex != -1 && index == 0 ) index = 1;
    
    var randomProjectIndex = makeUniqueRandom();
    if( randomProjectIndex == projectIndex ) randomProjectIndex = makeUniqueRandom();
    // verificamos si hay mas de un item
    if(numRandoms > 1){
      var numRandomsMo =  numRandoms - 1;
    }else {
      var numRandomsMo =  numRandoms;
    }
    if( index < numRandomsMo) {
      // aqui solo entra si la cantidad de item - 1 es mayor que el index 
      // cuando es un solo item aqui nunca entra
      if(numRandoms != 1) {
        projectsPreviewWrapper.eq(randomProjectIndex).toggleClass('slide-out', bool);
      }
      setTimeout( function(){
        //animate next preview project
        slideToggleProjects(projectsPreviewWrapper, projectIndex, index + 1, bool);
      }, 150);
    } else if ( index == numRandomsMo ) {
      if(numRandoms == 1){
        //this is the last project preview to be animated 
        projectsPreviewWrapper.eq(projectIndex).toggleClass('slide-out').one('transitionend', function(){

          if( projectIndex != -1) {
            projects.children('li.selected').addClass('content-visible');
            projectsPreviewWrapper.eq(projectIndex).addClass('slide-out');
          } else if( navigation.hasClass('nav-visible') && bool ) {
            navigation.addClass('nav-clickable');
          }
          projectsPreviewWrapper.off('transitionend');
          animating = false;
        });
      }else {
        //this is the last project preview to be animated 
        projectsPreviewWrapper.eq(randomProjectIndex).toggleClass('slide-out', bool).one('transitionend', function(){
          if( projectIndex != -1) {
            projects.children('li.selected').addClass('content-visible');
            projectsPreviewWrapper.eq(projectIndex).addClass('slide-out').removeClass('selected');
          } else if( navigation.hasClass('nav-visible') && bool ) {
            navigation.addClass('nav-clickable');
          }
          projectsPreviewWrapper.eq(randomProjectIndex).off('transitionend');
          animating = false;
        });
      }
    }
  }

  //http://stackoverflow.com/questions/19351759/javascript-random-number-out-of-5-no-repeat-until-all-have-been-used
  function makeUniqueRandom() {
    var index = Math.floor(Math.random() * uniqueRandoms.length);
    var val = uniqueRandoms[index];
    // now remove that value from the array
    uniqueRandoms.splice(index, 1);
    return val;
  }

  function createArrayRandom() {
    //reset array
    uniqueRandoms.length = 0;
    for (var i = 0; i < numRandoms; i++) {
      uniqueRandoms.push(i);
    }
  }

});

 /*
 * BG Loaded
 * Copyright (c) 2014 Jonathan Catmull
 * Licensed under the MIT license.
 */
 (function($){
   $.fn.bgLoaded = function(custom) {
     var self = this;

    // Default plugin settings
    var defaults = {
      afterLoaded : function(){
        this.addClass('bg-loaded');
      }
    };

    // Merge default and user settings
    var settings = $.extend({}, defaults, custom);

    // Loop through element
    self.each(function(){
      var $this = $(this),
        bgImgs = $this.css('background-image').split(', ');
      $this.data('loaded-count',0);
      $.each( bgImgs, function(key, value){
        var img = value.replace(/^url\(["']?/, '').replace(/["']?\)$/, '');
        $('<img/>').attr('src', img).load(function() {
          $(this).remove(); // prevent memory leaks
          $this.data('loaded-count',$this.data('loaded-count')+1);
          if ($this.data('loaded-count') >= bgImgs.length) {
            settings.afterLoaded.call($this);
          }
        });
      });

    });
  };

})(jQuery);