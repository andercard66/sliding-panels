<?php

  // desactivar barra wordpress
  add_filter( 'show_admin_bar', '__return_false' );
  // si el tema tiene traduccion a su idioma lo ahora automatico
  load_theme_textdomain( 'slidingpanels' );
  // permitir que wordpress agregue el title
  add_theme_support( 'title-tag' );
  // Agregue publicaciones predeterminadas y comentarios enlaces de fuentes RSS a la cabeza.
  add_theme_support( 'automatic-feed-links' );
  // soporte para imagen destacada
  add_theme_support( 'post-thumbnails' );
  // menus
  register_nav_menus( array(
    'primary' => __( 'Primary menu', 'slidingpanels' ),
    'social' => __( 'Social menu', 'slidingpanels' ),
  ));
  // soporte de html5
  add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );  

  // custom logo
  function theme_prefix_setup() {
    add_theme_support( 'custom-logo', array(
      'height'      => 140,
      'width'       => 500,
      'flex-width' => true,
    ) );
  }
  add_action( 'after_setup_theme', 'theme_prefix_setup' );

  // add more buttons to the html editor
  function sliding_add_quicktags() {
    if (wp_script_is('quicktags')){
  ?>
    <script type="text/javascript">
        QTags.addButton( 'sliding_pre', 'pre', '<pre>', '</pre>', 'pre', 'Code Block', 1, '' );
    </script>
  <?php
    }
  }
  add_action( 'admin_print_footer_scripts', 'sliding_add_quicktags' );

  /**
   *  Agregamos la primera clase del menu en la etiqueta para mostrar el icono d ela red social (solo funciona si se usa el menu social)
  */

  function add_menu_atts( $atts, $item, $args ) {

    if($args->theme_location == 'social'){
      $atts['class'] = $item->classes[0] . ' text-hidden';
      return $atts;
    }

    if($args->theme_location == 'primary'){
      $atts['class'] = 'menu-primcipal__item';
      return $atts;
    }

  }
  add_filter( 'nav_menu_link_attributes', 'add_menu_atts', 10, 3 );


  // scripts y css
  require get_template_directory() . '/inc/css_js.php';
  // customizar
  require get_template_directory() . '/inc/customizar.php';
  // metaboxes
  if( file_exists(dirname( __FILE__ ) . '/inc/cmb2/init.php' ) ){
    require_once dirname(__FILE__) . '/inc/cmb2/init.php';
  }
  require get_template_directory() . '/inc/metaboxes.php';
  require get_template_directory() . '/inc/media_gallery.php';