<?php
/**
 * Pagina general
*/

  get_header();

  while(have_posts()): the_post();
  $idpost = $post->ID;
?>

<a class="cd-nav-trigger cd-text-replace" href="#primary-nav">Menu<span aria-hidden="true" class="cd-icon"></span></a>

<div class="cd-project-page-container">

  <a class="home-logo" href="<?php echo home_url(); ?>">
    <?php
      $custom_logo_id = get_theme_mod( 'custom_logo' );
      $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
    ?>
      <img src="<?php echo $image[0]; ?>" alt="">
  </a>

  <div class="cd-project-page-header" style="background-image: url('<?php echo get_the_post_thumbnail_url( get_the_ID(), 'full' ) ?>')">
    <h1> <?php the_title( ) ?> </h1>
  </div>
  <div class="cd-project-page">
    <?php
      the_content();
    ?>
  </div>

  <div class="page-project-page-footer">
    <div class="footer-copyright">
      <p>&copy; <?php echo date("Y") ?> <a href="<?php echo esc_url( home_url() ); ?>" title="<?php esc_attr( bloginfo('name') ); ?>"><?php bloginfo('name'); ?></a>. <?php _e('All rights reserved.','slidingpanels'); ?></p>
      <p><?php echo __( 'Theme by', 'slidingpanels' ) ?> <a href="https://www.facebook.com/andercard22" target="_black">Andersson Mesa</a></p>
    </div>
    <div class="footer-menus">
      <?php if(has_nav_menu( 'social' )){
        wp_nav_menu( array(
          'theme_location' => 'social',
          'container' => 'nav', 
          'container_class' => '',
          'container_id' => 'social-nav-footer'
        ) );
      } ?>
    </div>
  </div>

</div>

<div class="cd-primary-nav">
  <?php
    if(has_nav_menu( 'primary' )) {
      $args = array(
        'theme_location' => 'primary',
        'container' => 'nav', 
        'container_class' => '',
        'container_id' => 'primary-nav'
      );
      wp_nav_menu( $args );
    }
  ?>
  <div class="cd-primary-nav__social">
    <?php if(has_nav_menu( 'social' )){
      wp_nav_menu( array(
        'theme_location' => 'social',
        'container' => 'nav', 
        'container_class' => '',
        'container_id' => 'social-nav-footer'
      ) );
    } ?>
  </div>
</div>


<?php 
  endwhile; 

  get_footer();

?>