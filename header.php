<?php

  wp_head();

?>

<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <?php
    $cont = 0;
    $paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;
    $per_page = (int) get_theme_mod( 'opciones_tema_vista', '2' );
    $estado_menu = get_theme_mod( 'opciones_tema_menu', 'si' );
    $args = array(
      'posts_per_page' => $per_page,
      'post_type' => 'post',
      'paged' => $paged,
      'orderby' => 'ASC'
    );
    $query = new WP_Query( $args );
    while($query->have_posts()): $query->the_post();
    $idpost = $post->ID;
    $cont = $cont+1;
      $items = $cont;
    endwhile;
  ?>
  <style>
    .cd-project-title {
      <?php if($items == 1 || $items == 0){ ?>
        transform: translateY(0);
      <?php }else{ ?>
        transform: translateY(<?php echo 100*(($items/2)/$items - $items/2)/$items ?>vh);
      <?php } ?>
    }
    .cd-projects-previews li:nth-of-type(2) .cd-project-title, 
    .cd-projects > li:nth-of-type(2) .cd-project-title {
      transform: translateY(<?php echo 100*(($items/2)/$items - $items/2 + 1)/$items ?>vh);
    }

    .cd-projects-previews li:nth-of-type(3) .cd-project-title, 
    .cd-projects > li:nth-of-type(3) .cd-project-title  {
      transform: translateY(<?php echo 100*(($items/2)/$items - $items/2 + 2)/$items ?>vh);
    }
    
    .cd-projects-previews li{
      min-height: <?php echo 100/$items ?>%;
    }

    .cd-projects-previews li:nth-of-type(2) a {
      transform: translateY(<?php echo -100/$items ?>%);
    }

    .cd-projects-previews li:nth-of-type(3) a {
      transform: translateY(<?php echo -100*2/$items ?>%);
    }

    @media (min-width: 1024px){
      .cd-project-title {
        width: <?php echo 100/$items ?>%;
        top: 50vh;
        transform: translateX(0%);
      }
      li.selected .cd-project-title,
      .cd-projects-previews li:nth-of-type(2) .cd-project-title, 
      .cd-projects > li:nth-of-type(2) .cd-project-title {
        transform: translateX(100%);
      }
      .cd-projects-previews li:nth-of-type(3) .cd-project-title, 
      .cd-projects > li:nth-of-type(3) .cd-project-title {
        transform: translateX(200%);
      }

      .cd-projects-previews li{
        min-height: <?php echo 100/$items ?>%;
      }

      /* width equal to window width */
      .cd-projects-previews a{
        width: <?php echo 100*$items ?>%;
      }

      .cd-projects-previews li:nth-of-type(2) a {
        transform: translateX(<?php echo -100/$items ?>%);
      }

      .cd-projects-previews li:nth-of-type(3) a {
        transform: translateX(<?php echo -100*2/$items ?>%);
      }


      li.selected .cd-project-title,
      .cd-projects > li:nth-of-type(2).selected .cd-project-title,
      .cd-projects > li:nth-of-type(3).selected .cd-project-title {
        transform: translateX(<?php echo (50*$items)-50 ?>%);
      }


    }
  <?php
    if($estado_menu == 'no' ){
  ?>
      .cd-nav-trigger {
        visibility: hidden;
      }
      .cd-nav-trigger.project-open {
        visibility: visible;
      }
  <?php } ?>
  </style>
  <body <?php body_class() ?> >