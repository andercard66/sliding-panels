<?php
  get_header();
?>

<div class="error404">
  <div class="error404__contenido">
    <p>No encontramos nada</p>
    <a class="error404__btn" href="<?php echo home_url() ?>">
      Ir al Inicio
    </a>
  </div>
</div>