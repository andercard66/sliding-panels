<?php

  if( !function_exists(css_scripts) ):
  function css_scripts()
  {
    /**
     * Css
    */
    
    // vendor css
    wp_enqueue_style( 'vendorcss', get_template_directory_uri() . '/css/vendorcss.min.css', array(), '1.0' );
    // fuentes
    wp_enqueue_style( 'font', 'http://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,700,700i|Raleway:400,400i,700', array(), '1.0' );
    // main css
    wp_enqueue_style( 'style', get_stylesheet_uri() );

    /**
     * Js
    */
    wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.js', array(), '1.0', false );
    wp_enqueue_script( 'jquery' );
    // vendors js
    wp_enqueue_script( 'vendorjs', get_template_directory_uri() . '/js/vendorjs.min.js', array('jquery'), '1.0', true );
    // main
    wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.min.js', array('jquery'), '1.0', true );
  }
  endif;

  add_action('wp_enqueue_scripts', 'css_scripts');


  // script admin
  if( !function_exists(css_scripts_admin) ):
    function css_scripts_admin(){
      // css admin
      wp_enqueue_style( 'style-admin', get_template_directory_uri() . '/style-admin.css', array(), '1.0' );
    }
  endif;

  add_action('admin_enqueue_scripts', 'css_scripts_admin');