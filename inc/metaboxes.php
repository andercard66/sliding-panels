<?php

// field proyect
add_action( 'cmb2_admin_init', 'sliding_field_proyect' );
function sliding_field_proyect() {

  $prefix = 'sliding_proyect_';

  $proyect = new_cmb2_box( array(
    'id'            => $prefix . 'metabox_proyect',
    'title'         => __( 'Additional', 'slidingpanels' ),
    'object_types'  => array( 'post' ), // Post type
  ) );

  $proyect->add_field( array(
    'name'    => esc_html__( 'Description', 'slidingpanels' ),
    'id'      => $prefix . 'description',
    'type'    => 'text',
  ) );
  
}