<?php

function themeslug_customize_register( $wp_customize ) {
  // Opciones del tema
  $wp_customize->add_section( 'opciones_tema' , array(
    'title'      => __( 'Opciones de temas', 'slidingpanels' ),
    'priority'   => 20,
) );

  $wp_customize->add_setting( 'opciones_tema_vista' , array(
      'default'        => '2',
      'capability'     => 'edit_theme_options',
      // 'transport' => 'postMessage',
      'type'           => 'theme_mod',
  ));

  $wp_customize->add_control( 'opciones_tema_vista', array(
  'label' => 'Proyectos por vista',
  'section' => 'opciones_tema',
  'settings' => 'opciones_tema_vista',
  'type' => 'select',
  'choices' => array(
    '1' => '1',
    '2' => '2 (Recomendado)',
    '3' => '3',
  ),
  ));

  // mostrar menu 

  $wp_customize->add_setting( 'opciones_tema_menu' , array(
      'default'        => 'si',
      'capability'     => 'edit_theme_options',
      // 'transport' => 'postMessage',
      'type'           => 'theme_mod',
  ));

  $wp_customize->add_control( 'opciones_tema_menu', array(
  'label' => 'Mostrar Menu',
  'section' => 'opciones_tema',
  'settings' => 'opciones_tema_menu',
  'type' => 'select',
  'choices' => array(
    'si' => 'Si',
    'no' => 'No',
  ),
  ));
}
add_action( 'customize_register', 'themeslug_customize_register' );