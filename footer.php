<?php
  wp_footer();
?>

<script>
  (function($){
  // galeria
    $(document).ready(function() {
      <?php
        $paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;
        $per_page = (int) get_theme_mod( 'opciones_tema_vista', '2' );
        $args = array(
          'posts_per_page' => $per_page,
          'post_type' => 'post',
          'paged' => $paged,
          'orderby' => 'ASC'
        );
        $query = new WP_Query( $args );
        while($query->have_posts()): $query->the_post();
        $idpost = $post->ID;
      ?>
        // galeria 
        $(".sliding_galleryid-<?php echo $idpost ?>").lightGallery();
      <?php endwhile; ?>
    });
  })(jQuery);
</script>

</body>
</html>