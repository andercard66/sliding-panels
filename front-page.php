<?php
  get_header();
?>

  <a class="cd-nav-trigger cd-text-replace" href="#primary-nav">Menu<span aria-hidden="true" class="cd-icon"></span></a>

  <a class="home-logo" href="<?php echo home_url(); ?>">
    <?php
      $custom_logo_id = get_theme_mod( 'custom_logo' );
      $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
    ?>
      <img src="<?php echo $image[0]; ?>" alt="">
  </a>

  <div class="cd-projects-container">
    <ul class="cd-projects-previews">
      <?php
        $paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;
        $per_page = (int) get_theme_mod( 'opciones_tema_vista', '2' );
        $args = array(
          'posts_per_page' => $per_page,
          'post_type' => 'post',
          'paged' => $paged,
          'orderby' => 'ASC'
        );
        $query = new WP_Query( $args );
        while($query->have_posts()): $query->the_post();
        $idpost = $post->ID;
      ?>
        <li class="previews-item">
          <a href="#0"  style="background-image: url('<?php the_post_thumbnail_url( $idpost, 'full' ) ?>')">
            <div class="cd-project-title">
              <h2> <?php the_title(); ?> </h2>
              <?php
                $description = get_post_meta( $idpost , 'sliding_proyect_description', true );
                if(isset($description)){
                  echo "<p>  {$description} </p>";
                }
              ?>
            </div>
          </a>
        </li>
      <?php endwhile; ?>
    </ul> 
    <!-- .cd-projects-previews -->

    <ul class="cd-projects">
      <?php
        $paged = ( get_query_var( 'page' ) ) ? absint( get_query_var( 'page' ) ) : 1;
        $per_page = (int) get_theme_mod( 'opciones_tema_vista', '2' );
        $args = array(
          'posts_per_page' => $per_page,
          'post_type' => 'post',
          'paged' => $paged,
          'orderby' => 'ASC'
        );
        $query = new WP_Query( $args );
        while($query->have_posts()): $query->the_post();
        $idpost = $post->ID;
      ?>
        <li class="cd-projects-item">
          <div class="preview-image" style="background-image: url('<?php the_post_thumbnail_url( $idpost, 'full' ) ?>')">
            <div class="cd-project-title">
              <h2> <?php the_title(); ?> </h2>
              <?php
                $description = get_post_meta( $idpost , 'sliding_proyect_description', true );
                if(isset($description)){
                  echo "<p>  {$description} </p>";
                }
              ?>
            </div> 
          </div>

          <div class="cd-project-info">
            <div class="cd-project-info_content">
              <?php the_content(); ?>
            </div>
          </div> 
          <!-- .cd-project-info -->

          <div class="cd-project-page-footer">
            <div class="footer-copyright">
              <p>&copy; <?php echo date("Y") ?> <a href="<?php echo esc_url( home_url() ); ?>" title="<?php esc_attr( bloginfo('name') ); ?>"><?php bloginfo('name'); ?></a>. <?php _e('All rights reserved.','slidingpanels'); ?></p>
              <p><?php echo __( 'Theme by', 'slidingpanels' ) ?> <a href="https://www.facebook.com/andercard22" target="_black">Andersson Mesa</a></p>
            </div>
            <div class="footer-menus">
              <?php if(has_nav_menu( 'social' )){
                wp_nav_menu( array(
                  'theme_location' => 'social',
                  'container' => 'nav', 
                  'container_class' => '',
                  'container_id' => 'social-nav-footer'
                ) );
              } ?>
            </div>
          </div>

        </li>
      <?php endwhile; ?>

    </ul> 
    <!-- .cd-projects -->

    <button class="scroll cd-text-replace">Scroll</button>
  </div> 
  <!-- .cd-project-container -->
  <div class="pagination">
    <?php 
      $big = 999999999; // need an unlikely integer

      echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'mid_size'   => 1,
        'prev_text'          => __('Previous', 'slidingpanels'),
        'next_text'          => __('Next', 'slidingpanels'),
        'current' => max( 1, get_query_var('page') ),
        'total' => $query->max_num_pages
      ) );
      
    ?>
  </div>
  <div class="cd-primary-nav">
    <?php
      if(has_nav_menu( 'primary' )) {
      $args = array(
        'theme_location' => 'primary',
        'container' => 'nav', 
        'container_class' => '',
        'container_id' => 'primary-nav'
      );
      wp_nav_menu( $args );
     }
    ?>
    <div class="cd-primary-nav__social">
      <?php if(has_nav_menu( 'social' )){
        wp_nav_menu( array(
          'theme_location' => 'social',
          'container' => 'nav', 
          'container_class' => '',
          'container_id' => 'social-nav-footer'
        ) );
      } ?>
    </div>
  </div>


<?php
  get_footer( );
?>